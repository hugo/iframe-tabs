var activeTabBtn = null;
var activeTab = null;
var tabid = 0;

var btndiv;
var tabs = []
var tabdiv;

function setActiveBtn(btn) {
	if (activeTabBtn != null) {
		activeTabBtn.classList.remove("active-tab-btn");
	}
	activeTabBtn = btn;
	activeTabBtn.classList.add("active-tab-btn");
}

function setActiveTab(tab) {
	if (activeTab != null) {
		activeTab.classList.remove("active-tab");
	}
	activeTab = tab;
	activeTab.classList.add("active-tab");
}

function newtab() {
	const id = tabid++;
	var btnel = document.getElementsByClassName("tab-btn-template")[0].cloneNode(true);
	btnel.classList = [];
	btnel.id = "tab-btn-" + id;
	btnel.innerText = "Tab " + id;
	btnel.onclick = function () { switchtab(id); };
	btndiv.append(btnel);
	setActiveBtn(btnel);

	var tabel = document.getElementsByClassName("tab-template")[0].cloneNode(true);
	tabel.classList = ["tab"];
	tabel.id = "tab-" + id;

	setActiveTab(tabel);

	tabel.querySelector("form").onsubmit = function (ev) {
		var url = tabel.querySelector("input[type=text]").value;
		if (url.substr(0, 4) != "http") {
			url = "http://" + url;
		}
		tabel.querySelector("iframe").setAttribute("src", url);
		ev.preventDefault();
	};

	tabdiv.append(tabel);

	tabs.push({id: id, btn: btnel, tab: tabel});
}

function switchtab(id) {
	const el = tabs.find( el => el.id == id );
	setActiveTab(el.tab);
	setActiveBtn(el.btn);
}

window.onload = function () {
	btndiv = document.getElementById("tab-btns");
	tabdiv = document.getElementById("contents");

	document.getElementById("new-tab-btn").click()
	var url;
	if (document.location.href.substr(-10) == "index.html") {
		url = document.location.href.substr(0, document.location.href.length - 10) + "about.html";
	} else {
		url = document.location.href + "about.html"
	}
	const tabel = document.getElementById("tab-0");
	tabel.querySelector("input[type=text]").value = url;
	tabel.querySelector("input[type=submit").click();
}
